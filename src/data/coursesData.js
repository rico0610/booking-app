const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi officia mollitia reiciendis exercitationem deserunt, nesciunt suscipit qui nihil expedita alias unde praesentium assumenda voluptatem dicta odio quidem, molestias inventore sint!",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "PHP-Django",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi officia mollitia reiciendis exercitationem deserunt, nesciunt suscipit qui nihil expedita alias unde praesentium assumenda voluptatem dicta odio quidem, molestias inventore sint!",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "PHP-Springboot",
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi officia mollitia reiciendis exercitationem deserunt, nesciunt suscipit qui nihil expedita alias unde praesentium assumenda voluptatem dicta odio quidem, molestias inventore sint!",
        price: 55000,
        onOffer: true
    }
]

export default coursesData;